#if !defined(__AES128_CBC_H__)
#define __AES128_CBC_H__

#include "aes128.h"

void aes128_cbc_init(uint8_t* key, uint8_t* init_vec);
void aes128_cbc_encrypt(uint8_t* data, size_t len);
void aes128_cbc_decrypt(uint8_t* data, size_t len);

#endif // __AES128_CBC_H__
