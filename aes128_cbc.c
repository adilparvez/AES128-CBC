#include <string.h>

#include "aes128_cbc.h"

static uint8_t* iv;


void aes128_cbc_init(uint8_t* key, uint8_t* init_vec) {
    aes128_init(key);
    iv = init_vec;
}


static void xor_equals(uint8_t* lblock, uint8_t* rblock) {
    for (size_t i = 0; i < 16; i++) {
        lblock[i] ^= rblock[i];
    }
}


void aes128_cbc_encrypt(uint8_t* data, size_t len) {
    xor_equals(&data[0], &iv[0]);
    aes128_encrypt(&data[0]);
    for (size_t blk_idx = 1; blk_idx < len/16; blk_idx++) {
        xor_equals(&data[blk_idx*16], &data[(blk_idx - 1)*16]);
        aes128_encrypt(&data[blk_idx*16]);
    }
}


void aes128_cbc_decrypt(uint8_t* data, size_t len) {
    uint8_t cipher_text_buff[16];
    uint8_t xor_buff[16];

    memcpy(&cipher_text_buff[0], &data[0], 16*sizeof(uint8_t));

    aes128_decrypt(&data[0]);
    xor_equals(&data[0], &iv[0]);
    for (size_t blk_idx = 1; blk_idx < len/16; blk_idx++) {
        memcpy(&xor_buff[0], &cipher_text_buff[0], 16*sizeof(uint8_t));
        memcpy(&cipher_text_buff[0], &data[blk_idx*16], 16*sizeof(uint8_t));
        aes128_decrypt(&data[blk_idx*16]);
        xor_equals(&data[blk_idx*16], &xor_buff[0]);
    }
}
