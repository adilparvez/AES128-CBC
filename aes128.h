#if !defined(__AES128_H__)
#define __AES128_H__

#include <stddef.h>
#include <stdint.h>

void aes128_init(uint8_t* key);
void aes128_encrypt(uint8_t* block);
void aes128_decrypt(uint8_t* block);

#endif // __AES128_H__
