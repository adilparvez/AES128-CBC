all: main

main: main.o aes128.o aes128_cbc.o
	clang -Wall -Wextra -std=c99 main.o aes128.o aes128_cbc.o -o main

main.o: main.c
	clang -Wall -Wextra -std=c99 -c main.c

aes128.o: aes128.c aes128.h
	clang -Wall -Wextra -std=c99 -c aes128.c

aes128_cbc.o: aes128_cbc.c aes128_cbc.h
	clang -Wall -Wextra -std=c99 -c aes128_cbc.c

.PHONY: clean

clean:
	rm -f main.o aes128.o aes128_cbc.o main
